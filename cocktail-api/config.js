
const path = require('path');

const rootPath = __dirname;


module.exports = {
    rootPath,
    uploadPath: path.join(rootPath, 'public/uploads'),
    dbUrl: 'mongodb://localhost/Cocktail',
    mongoOptions: {
        useNewUrlParser: true,
        useCreateIndex: true,
    },
    facebook: {
        appId: '324636778208405',
        appSecret: '2f757f3c1bf44fac4f10dcd60eec0eed'
    }
};