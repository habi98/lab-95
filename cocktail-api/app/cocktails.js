const express = require('express');
const multer = require('multer');
const nanoid = require('nanoid');
const path = require('path');
const config = require('../config');

const auth = require('../middleware/auth');
const tryAuth = require('../middleware/tryAuth');
const permit = require('../middleware/permit');

const Cocktail = require('../models/Cocktail');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname))
    }
});

const upload = multer({storage});

const router = express.Router();

router.get('/', tryAuth, (req, res) => {

        let criteria = {published: true};

        if (req.user && req.user.role === 'admin') {
            criteria = {}
        } else if(req.user && req.user.role === 'user') {
            criteria = {$or: [
               {published: true},
               {user: req.user._id}
          ]};
         }

        if (req.query.user) {
            criteria = {user: req.query.user}
        }


    Cocktail.find(criteria)
        .then(cocktails => res.send(cocktails))
        .catch(() => res.sendStatus(500))
});

router.get('/:id', tryAuth, (req, res) => {
    let criteria = {_id: req.params.id, published: true};

    if (req.user && req.user.role === 'admin') {
        criteria = {_id: req.params.id}
    }
    Cocktail.findOne(criteria)
        .then(cocktail => res.send(cocktail))
        .catch(() => res.sendStatus(500))
});


router.post('/', auth, upload.single('image'), async (req, res) => {
    let file = 'iamge.png';

    if (req.file) {
        file = req.file.filename
   }

    const cocktail = await new Cocktail({
        name: req.body.name,
        user: req.user._id,
        recipe: req.body.recipe,
        image: file,
        ingredients: JSON.parse(req.body.ingredients),
    });

    cocktail.save()
        .then(result => res.send(result))
        .catch(error => res.status(500).send(error))
});


router.post('/toggle_published/:id', [auth, permit('admin')], async (req, res) => {
    try {
        const cocktail = await Cocktail.findById(req.params.id);

        if (!cocktail) {
            return res.sendStatus(404);
        }

        cocktail.published = !cocktail.published;

        await cocktail.save();
        res.send(cocktail);
    } catch (e) {
        res.sendStatus(500)
    }

});

router.delete('/:id', [auth, permit('admin')], async (req, res) => {
    try {
        const cocktail = await Cocktail.findByIdAndDelete({_id: req.params.id});

        if (!cocktail) {
            return res.sendStatus(404)
        }

        await cocktail.save();
        res.status(200).send('deleted successfully')

    } catch (e) {
        res.sendStatus(500)
    }
});



module.exports = router;