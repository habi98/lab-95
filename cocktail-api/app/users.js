const express = require('express');
const axios = require('axios');

const config = require('../config');
const User = require('../models/User');

const router = express.Router();



router.post('/facebookLogin',   async (req, res) => {


    const inputToken = req.body.accessToken;
    const accessToken = config.facebook.appId + '|' + config.facebook.appSecret;
    const debugTokenUrl = `https://graph.facebook.com/debug_token?input_token=${inputToken}&access_token=${accessToken}`;

    try {
        const response = await axios.get(debugTokenUrl);

        const responseData = response.data;


        if (responseData.data.error) {
            return res.status(500).send({error: 'Token incorrect'});
        }
        if (responseData.data.user_id !== req.body.id) {
            return res.status(500).send({error: 'User is wrong'});
        }

        let user = await User.findOne({facebookId: req.body.id});



        if (!user) {
            user = new User({
                displayName: req.body.name,
                avatarImage: req.body.picture.data.url ,
                facebookId: req.body.id
            });
        }

        user.generateToken();

        await user.save();

         res.send(user);
    } catch (e) {
        return res.status(500).send({error: 'Something went wrong'});
    }
});

router.delete('/sessions', async (req, res) => {
    const token = req.get('Authorization');

    const success = {message: 'Logged out'};

    if (!token) {
        return res.send(success)
    }

    const user = await User.findOne({token});

    if (!user) {
        return res.send(success)
    }

    user.generateToken();
    await user.save();

    return res.send(success)
});


module.exports = router;