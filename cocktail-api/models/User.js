const mongoose = require('mongoose');
const nanoid = require('nanoid');

const Schema = mongoose.Schema;

const UserSchema = new Schema({
   displayName: {
       type: String,
       required: true
   },
    avatarImage: {
       type: String
   },
    facebookId: {
       type: String
   },
    token: {
        type: String,
        required: true
    },
   role: {
       type: String,
       required: true,
       default: 'user',
       enum: ['user', 'admin']
   }
});


UserSchema.methods.generateToken = function() {
    this.token = nanoid();
};

const User = mongoose.model('User', UserSchema);

module.exports = User;