const mongoose = require('mongoose');


const Schema = mongoose.Schema;

const CocktailSchema = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    name: {
        type: String,
        required: true
    },
    image: {
        type: String,
    },
    recipe: {
        type: String,
        required: true
    },
    published: {
        type: Boolean,
        default: false,
        required: true
    },
    ingredients: [{name: String, amount: String}]
});


const Cocktail = mongoose.model('Cocktail', CocktailSchema);


module.exports = Cocktail;