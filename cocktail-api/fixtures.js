const mongoose = require('mongoose');
const config = require('./config');
const nanoid = require('nanoid');
const User = require('./models/User');


const run = async () => {
    await mongoose.connect(config.dbUrl, config.mongoOptions);

    const connection = mongoose.connection;

    const collections = await connection.db.collections();

    for (let collection of collections) {
        await collection.drop();
    }


    await User.create(
          {
           displayName:  'Tyler Alcgaeddiijec Chengsky',
           avatarImage: 'https://platform-lookaside.fbsbx.com/platform/profilepic/?asid=103645877550562&height=50&width=50&ext=1561212854&hash=AeTMYSd0_d8zMGbQ',
           facebookId: '103645877550562',
           role: 'admin',
           token: nanoid()
         },
        {
            displayName:  'Sandra Alcgbhaghaech Occhinoescu',
            avatarImage: 'https://platform-lookaside.fbsbx.com/platform/profilepic/?asid=100550574531002&height=50&width=50&ext=1561211696&hash=AeR4qq6Tjq4HToaC',
            facebookId: '100550574531002',
            role: 'user',
            token: nanoid()
        },
        {
            displayName:  'Donna Alcgbaacaefce Seligsteinson',
            avatarImage: 'https://platform-lookaside.fbsbx.com/platform/profilepic/?asid=102143337702710&height=50&width=50&ext=1561214071&hash=AeQUF3QIU_4KKgrX',
            facebookId: '102143337702710',
            role: 'user',
            token: nanoid()
        },


    );

    await connection.close();
};

run().catch(error => {
    console.error('Something went wrong', error);
});
