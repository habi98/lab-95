import React, {Component, Fragment} from 'react';
import {fetchCocktail} from "../../store/actions";
import {connect} from "react-redux";
import {Card, CardBody, CardText} from "reactstrap";

class OneCocktail extends Component {
    componentDidMount() {
        const cocktailId = this.props.match.params.id;
        this.props.fetchCocktail(cocktailId);
    }

    render() {
        return (
          <Fragment>
                  {this.props.cocktail ? (
                      <Card>
                          <CardBody>
                      <Fragment>
                          <img className="d-inline-block pr-3" style={{width: '500px', height: '400px', verticalAlign: 'top'}} src={'http://localhost:8000/uploads/' + this.props.cocktail.image} alt="artist"/>
                          <CardBody className="d-inline-block">
                              <h2>{this.props.cocktail.name}</h2>
                              <CardText>Ингредиенты:</CardText>
                              {this.props.cocktail.ingredients.map(ing => (
                                  <div key={ing._id}>
                                      <span style={{display: 'block'}}>{ing.name} - {ing.amount}</span>
                                  </div>
                              ))}
                          </CardBody>
                          <CardBody className="d-inline-block">
                                <h3>Рецепт: </h3>
                                <CardText>{this.props.cocktail.recipe}</CardText>
                          </CardBody>
                      </Fragment>
                          </CardBody>
                      </Card>
                  ): null}
          </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    cocktail: state.cocktail
});

const mapDispatchToProps = dispatch => ({
    fetchCocktail: cocktailId => dispatch(fetchCocktail(cocktailId))
});

export default connect(mapStateToProps, mapDispatchToProps)(OneCocktail);