import React, {Component, Fragment} from 'react';
import nanoid from 'nanoid'
import Form from "reactstrap/es/Form";
import {Button, Col, FormGroup, Input, Label} from "reactstrap";
import {createCocktail} from "../../store/actions";
import {connect} from "react-redux";


class AddFormCocktail extends Component {
    state = {
        name: '',
        image: '',
        recipe: '',
        ingredients: [{name: '', amount: '', id: nanoid()}]
    };


    submitFormHandler = event => {
        event.preventDefault();

        const formData = new FormData();

        Object.keys(this.state).forEach(key => {
            if (key === 'ingredients') {
                formData.append(key, JSON.stringify(this.state[key]))
            } else
            formData.append(key, this.state[key])
        });

        this.props.createCocktail(formData).then(() => this.props.history.push('/'))
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        })
    };

    addIngredients = () => {
      this.setState({
          ingredients: [...this.state.ingredients, {name: '', amount: '', id: nanoid()}]
      })
    };

    removeIngredient = (ind) => {
        const ingredients = [...this.state.ingredients];

        ingredients.splice(ind, 1);

        this.setState({ingredients})
    };

    ingredientInputChangeHandler = (event, idx) => {
        const ingredient = {...this.state.ingredients[idx]};

        ingredient[event.target.name] = event.target.value;
        const ingredients = [...this.state.ingredients];

        ingredients[idx] = ingredient;
        this.setState({ingredients})
    };

    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        })
    };



    render() {
        return (
            <Form onSubmit={this.submitFormHandler}>
                <h2>Add new cocktail</h2>
                <FormGroup row>
                    <Label sm={2}>Name</Label>
                    <Col sm={10}>
                        <Input type="text" name="name" velue={this.state.name} onChange={this.inputChangeHandler}  placeholder="name" />
                    </Col>
                </FormGroup>

                <FormGroup row>

                    {this.state.ingredients.map((ing, ind) => (
                        <Fragment key={ing.id}>
                            <Label for="examplePassword" sm={2}>{ind <= 0 && <span>Ingredients</span>}</Label>
                            <Col sm={6} className="mb-3" >
                                <Input type="text"  name="name"  onChange={(event) => this.ingredientInputChangeHandler(event, ind)} placeholder="Ingredients name" />
                            </Col>
                            <Col sm={3}>
                                <Input type="text" name="amount"  onChange={(event) => this.ingredientInputChangeHandler(event, ind)}   placeholder="Amount" />
                            </Col>
                            <Col sm={1}>
                            {ind > 0 && <Button type="button" onClick={() => this.removeIngredient(ind)} style={{fontSize: '40px'}} close />}

                            </Col>
                        </Fragment>
                    ))}
                </FormGroup>
                <FormGroup row>
                    <Col sm={{offset: 2, size: 10}}>
                        <Button type="button" onClick={this.addIngredients} color="primary">Add ingredient</Button>
                    </Col>
                </FormGroup>

                <FormGroup row>
                    <Label  sm={2}>Recipe</Label>
                    <Col sm={10}>
                        <Input velue={this.state.recipe} onChange={this.inputChangeHandler}   type="textarea"  name="recipe"/>
                    </Col>
                </FormGroup>

                <FormGroup row>
                    <Label  sm={2}>Image</Label>
                    <Col sm={10}>
                        <Input type="file"  name="image" onChange={this.fileChangeHandler}/>
                    </Col>
                </FormGroup>

                <FormGroup row>
                    <Col sm={{offset: 2, size: 10}}>
                        <Button type="submit" color="primary">Create Cocktail</Button>
                    </Col>
                </FormGroup>

            </Form>
        );
    }
}

const mapDispatchToProps = dispatch => ({
    createCocktail: (cocktailData) => dispatch(createCocktail(cocktailData))
});

export default connect(null, mapDispatchToProps)(AddFormCocktail);