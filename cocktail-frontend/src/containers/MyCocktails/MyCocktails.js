import React, {Component} from 'react';
import {fetchCocktails} from "../../store/actions";
import {connect} from "react-redux";
import {Card, CardBody, CardText} from "reactstrap";

class MyCocktails extends Component {
    componentDidMount() {
        this.props.fetchCocktails(this.props.match.params.id);
    }

    render() {
        return (
            <div>
                <h2>My Cocktails</h2>
                {this.props.cocktails.map(cocktail => (
                    <Card className="mt-2" key={cocktail._id}>
                        <CardBody>
                                <img className="d-inline-block pr-3" style={{width: '500px', height: '400px', verticalAlign: 'top'}} src={'http://localhost:8000/uploads/' + cocktail.image} alt="artist"/>
                                <CardBody className="d-inline-block">
                                    <h2>{cocktail.name}</h2>
                                    <CardText>Ингредиенты:</CardText>
                                    {cocktail.ingredients.map(ing => (
                                        <div key={ing._id}>
                                            <span style={{display: 'block'}}>{ing.name} - {ing.amount}</span>
                                        </div>
                                    ))}
                                </CardBody>
                                <CardBody className="d-inline-block">
                                    <h3>Рецепт: </h3>
                                    <CardText>{cocktail.recipe}</CardText>
                                </CardBody>
                        </CardBody>
                    </Card>
                ))}
            </div>
        );
    }
}

const mapStateToProps = state => ({
    cocktails: state.cocktails
})

const mapDispatchToProps = dispatch => ({
   fetchCocktails: (userId) => dispatch(fetchCocktails(userId))
});

export default connect(mapStateToProps, mapDispatchToProps)(MyCocktails);