import React, {Component} from 'react';
import {deleteCocktail, fetchCocktails, publishCocktail} from "../../store/actions";
import {connect} from "react-redux";
import {Button, Card, CardBody, CardColumns, CardFooter, CardImg, CardTitle} from "reactstrap";
import {Link} from "react-router-dom";

class Cocktails extends Component {

    componentDidMount() {
        this.props.fetchCocktails()
    }

    render() {
        return (
            <div>
                <h2>Cocktails</h2>

                <CardColumns>
                    {this.props.cocktails.map(cocktail => (
                            <Card key={cocktail._id}>
                             <Link  to={'/cocktail/' + cocktail._id}>
                                <CardImg top width="100%" src={'http://localhost:8000/uploads/' + cocktail.image} alt="cocktail Image" />
                                <CardBody>
                                    <CardTitle>{cocktail.name}</CardTitle>
                                </CardBody>
                             </Link>
                                {this.props.user && this.props.user.role === 'admin' && (
                                    <CardFooter>
                                        {cocktail.published === false ? <Button type="button" onClick={() => this.props.publishCocktail(cocktail._id)} className="mr-2">Опубликовать</Button>: null}

                                        <Button color="danger" onClick={() => this.props.deleteCocktail(cocktail._id)}>удалить</Button>
                                    </CardFooter>
                                )}
                                {this.props.user &&
                                    this.props.user.role === 'user'
                                    && cocktail.published === false &&
                                    <CardFooter className="text-muted">
                                        Ваш коктейль находится на рассмотрении модератора
                                    </CardFooter>
                                }
                            </Card>
                    ))}
                </CardColumns>
            </div>
        );
    }
}

const mapStateToProps = state => ({
   cocktails: state.cocktails,
   user: state.user
});

const mapDispatchToProps = dispatch => ({
    fetchCocktails: () => dispatch(fetchCocktails()),
    publishCocktail: cocktailId => dispatch(publishCocktail(cocktailId)),
    deleteCocktail: cocktailId => dispatch(deleteCocktail(cocktailId))
});

export default connect(mapStateToProps, mapDispatchToProps)(Cocktails);