import React from 'react';
import Cocktails from "./containers/Cocktails/Cocktails";
import AddFormCocktail from "./containers/AddFormCocktail/AddFormCocktail";
import OneCocktail from "./containers/OneCocktail/OneCocktail";
import MyCocktails from "./containers/MyCocktails/MyCocktails";
import {Route, Switch, Redirect} from "react-router-dom";

const ProtectedRoute = ({isAllowed, ...props}) => (
    isAllowed ? <Route {...props}/> : <Redirect to="/"/>
);


const Routes = ({user}) => {
    let validate = (user && (user.role === 'user' || user.role === 'admin'));

    return (
        <Switch>
            <Route path="/" exact component={Cocktails}/>
            <ProtectedRoute isAllowed={validate} path="/add/cocktail" exact component={AddFormCocktail}/>
            <Route path="/cocktail/:id" exact component={OneCocktail}/>
            <ProtectedRoute isAllowed={validate} path="/my_cocktails/:id" exact component={MyCocktails}/>
        </Switch>
    );
};

export default Routes;