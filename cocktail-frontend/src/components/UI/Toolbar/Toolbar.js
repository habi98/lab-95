import React from 'react';
import {
    Collapse,
    Nav,
    Navbar,
    NavbarBrand,
    NavbarToggler,
} from "reactstrap";

import {NavLink as RouterNavLink} from "react-router-dom";
import UserMenu from "./Menus/UserMenu";
import FacebookLogin from "../../FacebookLogin/FacebookLogin";

const Toolbar = ({user, logout}) => {
    return (
        <Navbar color="light" light expand="md">
            <NavbarBrand tag={RouterNavLink} to="/">Cocktail Builder</NavbarBrand>
            <NavbarToggler/>
            <Collapse navbar>
                <Nav className="ml-auto" navbar>
                    {user ? <UserMenu user={user} logout={logout}/>: <FacebookLogin/>}
                </Nav>
            </Collapse>
        </Navbar>
    );
};

export default Toolbar;