import React, {Fragment} from 'react';
import {DropdownItem, DropdownMenu, DropdownToggle, NavItem, NavLink, UncontrolledDropdown} from "reactstrap";
import {NavLink as RouterNavLink} from "react-router-dom";

const styles = {
    height: '40px',
    borderRadius: '50%',
    display: 'inline-block'
};

const UserMenu = ({user, logout}) => {
    return (
        <Fragment>
            <NavItem>
                <NavLink tag={RouterNavLink} to="/add/cocktail">Add New Cocktail</NavLink>
            </NavItem>
            <UncontrolledDropdown nav inNavbar>
                <DropdownToggle nav caret className="d-inline-block">
                    {user.displayName}
                </DropdownToggle>
                <img src={user.avatarImage} style={styles} alt="avatar"/>
                <DropdownMenu right>
                    <NavLink className="p-0" tag={RouterNavLink} to={"/my_cocktails/" + user._id}>
                        <DropdownItem>
                            My cocktails
                        </DropdownItem>
                    </NavLink>
                    <DropdownItem onClick={logout}>
                        Logout
                    </DropdownItem>
                </DropdownMenu>
            </UncontrolledDropdown>
        </Fragment>
    );
};

export default UserMenu;