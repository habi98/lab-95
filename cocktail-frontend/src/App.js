import React, {Component, Fragment} from 'react';
import Toolbar from "./components/UI/Toolbar/Toolbar";
import Container from "reactstrap/es/Container";
import {connect} from "react-redux";

import './App.css'
import {NotificationContainer} from "react-notifications";
import {logoutUser} from "./store/actions";
import Routes from "./Routes";

class App extends Component{
    render() {
        return (
            <Fragment>
                <Toolbar user={this.props.user} logout={this.props.logoutUser}/>
                <NotificationContainer/>
                <Container>
                    <Routes user={this.props.user}/>
                </Container>
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    user: state.user
});

const mapDispatchToProps = dispatch => ({
    logoutUser: () => dispatch(logoutUser())
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
