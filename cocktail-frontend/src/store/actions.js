import axios from '../axios-cocktail-api'
import {NotificationManager} from 'react-notifications'

export const LOGIN_USER_SUCCESS = 'LOGIN_USER_SUCCESS';
export const LOGIN_USER_FAILURE = 'LOGIN_USER_FAILURE';

export const CREATE_COCKTAIL_SUCCESS = 'CREATE_COCKTAIL_SUCCESS';
export const FETCH_COCKTAIL_SUCCESS = 'FETCH_COCKTAIL_SUCCESS';
export const FETCH_COCKTAIL_ID_SUCCESS = 'FETCH_COCKTAIL_ID_SUCCESS';
export const PUBLISH_COCKTAILS_SUCCESS = 'PUBLISH_COCKTAILS_SUCCESS';
export const DELETE_COCKTAILS_SUCCESS = 'DELETE_COCKTAILS_SUCCESS';
export const LOGOUT_USER = 'LOGOUT_USER';


export const fetchCocktailSuccess = (cocktails) => ({type: FETCH_COCKTAIL_SUCCESS, cocktails});
export const createCocktailSuccess = () => ({type: CREATE_COCKTAIL_SUCCESS});
export const fetchCocktailIdSuccess = (cocktail) => ({type: FETCH_COCKTAIL_ID_SUCCESS, cocktail});
export const loginUserSuccess = user => ({type: LOGIN_USER_SUCCESS, user});
export const loginUserFailure = error => ({type: LOGIN_USER_FAILURE, error});
export const publishCocktailSuccess = () => ({type: PUBLISH_COCKTAILS_SUCCESS});
export const deleteCocktailsSuccess = () => ({type: DELETE_COCKTAILS_SUCCESS});


export const createCocktail = (cocktailData, history )=> {
    return dispatch => {
        return axios.post('/cocktails', cocktailData).then(
            () => {
                dispatch(createCocktailSuccess());
            }
        )
    }
};

export const fetchCocktails = (userId) => {
    let url = '/cocktails';

   if (userId) {
       url += `?user=${userId}`
   }
   return (dispatch) => {
       axios.get(url).then(
           response => {
               dispatch(fetchCocktailSuccess(response.data))
           }
       )
   }
};

export const facebookLogin = userData => {
    return (dispatch) => {
        return axios.post('/users/facebookLogin', userData,).then(
            response => {
                dispatch(loginUserSuccess(response.data));
                dispatch(fetchCocktails());
                NotificationManager.success('Logged in via Facebook');
            },
            () => {
                dispatch(loginUserFailure('Login via Facebook failed'));
            }
        )
    }
};

export const fetchCocktail = (cocktailId) => {
   return dispatch => {
       axios.get('/cocktails/' + cocktailId).then(
           response =>{
               dispatch(fetchCocktailIdSuccess(response.data))
           }
       )
   }
};



export const publishCocktail = (cocktailId) => {
    return dispatch  => {
        axios.post("/cocktails/toggle_published/" + cocktailId).then(
            () => {
                dispatch(publishCocktailSuccess());
                dispatch(fetchCocktails())
            }
        )
    }
};

export const deleteCocktail = (cocktailId) => {
    return dispatch => {
        axios.delete('/cocktails/' + cocktailId).then(
            () => {
                dispatch(deleteCocktailsSuccess());
                dispatch(fetchCocktails())
            }
        )
    }
};

export const logoutUser = () => {
    return (dispatch, getState) => {
        const token = getState().user.token;
        const config = {headers: {'Authorization': token}};

        return axios.delete('/users/sessions', config).then(
            () =>{
                dispatch({type: LOGOUT_USER});
                dispatch(fetchCocktails());
                NotificationManager.success('Logget out!')
            },
            error => {
                NotificationManager.error('Could not logout')
            }
        )
    }
};









