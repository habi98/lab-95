import {FETCH_COCKTAIL_ID_SUCCESS, FETCH_COCKTAIL_SUCCESS, LOGIN_USER_SUCCESS, LOGOUT_USER} from "./actions";


const initialState = {
    user: null,
    cocktails: [],
    cocktail: null
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case LOGIN_USER_SUCCESS:
            return {...state, user: action.user};
        case FETCH_COCKTAIL_SUCCESS:
            return {...state, cocktails: action.cocktails};
        case FETCH_COCKTAIL_ID_SUCCESS:

            let cock = action.cocktail;
            if (action.cocktail === '') {
                cock = null
            }
            return {...state, cocktail: cock};
        case LOGOUT_USER:
            return {...state, user: null};
        default:
            return state
    }
};

export default reducer;